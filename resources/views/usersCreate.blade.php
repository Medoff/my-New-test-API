<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

</head>
<body>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
{{--<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>--}}
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading"></div>
                <div class="panel-body">
                    <div id="errorsBlock" class="errorsBlock"></div>

                    <div class="form-group">
                        <label for="userPhoto">Фото</label>
                        <input type="file" id="userPhoto" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="userName">Имя</label>
                        <input type="text" class="form-control" id="userName">
                    </div>

                    <div class="form-group">
                        <label for="userEmail">Email</label>
                        <input type="text" class="form-control" id="userEmail">
                    </div>

                    <div class="form-group">
                        <label for="userPhone">Телефон</label>
                        <input type="tel" class="form-control" id="userPhone">
                    </div>

                    <div class="form-group">
                        <label for="userPositionId">Должность</label>
                        <select class="form-control" id="userPositionId">
                            @foreach($positions as $position)
                                <option value="{{ $position->id }}">{{ $position->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <button class="btn btn-sm btm-primary" id="registerUserButton">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', (e) => {
        let buttonSave = document.getElementById('registerUserButton');

        if (buttonSave) {
            buttonSave.addEventListener('click', () => {
               let token = null;
               const name = document.getElementById('userName').value;
               const email = document.getElementById('userEmail').value;
               const phone = document.getElementById('userPhone').value;
               const position_id = document.getElementById('userPositionId').value;
               const photo = document.getElementById('userPhoto').files[0];

               let formData = new FormData();
                formData.append('name', name);
                formData.append('email', email);
                formData.append('phone', phone);
                formData.append('position_id', position_id);
                formData.append('img', photo);

                fetch(location.protocol + '//' + location.host + '/api/v1/token').then(function(response) {
                    return response.json();
                }).then(function(data) {
                    token = data.token

                    return fetch(location.protocol + '//' + location.host + '/api/v1/users', {
                        method: 'POST',
                        body: formData,
                        headers: {
                            'Accept': 'application/json',
                            'Authorization': 'Bearer ' + token,
                            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
                        },
                    });
                }).then(function(response) {
                    return response.json();
                }).then(function(data) {
                    if(data.success) {
                        window.location.href = location.protocol + '//' + location.host + '/api/v1/users';
                    } else {
                        if (data.fails) {
                            let errorsBlock = document.querySelector('.errorsBlock');
                            if(errorsBlock) {
                                while (errorsBlock.firstChild) {
                                    errorsBlock.removeChild(errorsBlock.firstChild);
                                }
                            }
                            for (const property in data.fails) {
                                if(data.fails.hasOwnProperty(property) && data.fails[property].length > 0) {
                                    for (let i=0; i < data.fails[property].length; i++) {
                                        appendHtml(errorsBlock, '<p class="text-danger">' + data.fails[property][i] + '</p>');
                                    }
                                }
                            }
                        }
                    }
                }).catch(function(error) {});
            });
        }
    });

    function appendHtml(el, str) {
        let div = document.createElement('div');
        div.innerHTML = str;
        while (div.children.length > 0) {
            el.appendChild(div.children[0]);
        }
    }
</script>
</body>
</html>
