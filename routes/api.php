<?php
use App\Http\Controllers\Api\PositionController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Public routes
Route::group([
    'prefix'=>'v1',
],function(){
    Route::get('positions',[PositionController::class,'index']);
    Route::get('users',[UserController::class,'index']);
    Route::get('users/{id}',[UserController::class,'show']);
    Route::get('token',[AuthController::class,'token'])->name ('getToken');

    //Sanctum protected routes
    Route::group(['middleware'=>['auth:sanctum']],function(){
        Route::post('users',[UserController::class,'create']);
    });
});




//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});
