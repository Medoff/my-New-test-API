<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $response = [
            'success' => true,
        ];
        $count = $request->input('count') ?? 5;
        $offset = $request->input('offset');

        $validator = Validator::make($request->all(), [
            'count' => 'integer',
            'page' => 'integer|min:1',
            'offset' => 'integer|min:0',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation failed',
                'fails' => $validator->messages(),
            ], 422);
        }
        if (isset($offset) && $offset > 0) {
            $ids = User::orderBy('id')->take($offset)->pluck('id');
            if ($ids) {
                $users = User::orderBy('id', 'asc')->whereNotIn('id', $ids)->paginate($count)->withQueryString();
            } else {
                $users = User::orderBy('id', 'asc')->paginate($count)->withQueryString();
            }
        } else {
            $users = User::orderBy('id', 'asc')->paginate($count)->withQueryString();
        }

        if ($request->input('page') && $request->input('page') > $users->lastPage()) {
            return response()->json([
                'success' => false,
                'message' => 'Page not found'
            ], 404);
        }

        $response['page'] = $users->currentPage();
        $response['total_pages'] = $users->lastPage();
        $response['total_users'] = $users->total();
        $response['count'] = $count;
        $response['links'] = [
            'next_url' => $users->nextPageUrl(),
            'prev_url' => $users->previousPageUrl(),
        ];

        $filtered = $users->map(function ($user) {
            return [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
                'position' => $user->position ? $user->position->name : null,
                'position_id' => $user->position ? $user->position->id : null,
                'registration_timestamp' => null,
                'photo' => $user->img && str_contains($user->img, 'http') ? $user->img : url('storage/' . $user->img)
            ];
        });

        $response['users'] = $filtered;

        return response()->json(['data' => $response, 200]);
    }


    public function create(Request $request)
    {
        $phonePattern = "/[\+]{0,1}380([0-9]{9})/";

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|min:2|max:60',
            'email' => 'required|string|min:2|max:60',
            'phone' => 'required|string|regex:'.$phonePattern,
            'position_id' => 'required|integer|min:1',
            'img'=>'required|max:5120|mimes:jpeg,jpg|dimensions:min_width=70,min_height=70'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'Validation failed',
                'fails' => $validator->messages(),
            ], 422);
        }
        $user = User::where('email', $request->input('email'))->orWhere('phone', $request->input('phone'))->first();
        if ($user) {
            return response()->json([
                "success" => false,
                "message" => "User with this phone or email already exist",
            ], 409);
        }

        $optimizedImagePath = $this->imgResize($request->file('img'));

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt('11111111'),
            'phone' => $request->input('phone'),
            'position_id' => $request->input('position_id'),
            'img' => $optimizedImagePath,
        ]);

        $authUser = Auth::user();
        $authUser->currentAccessToken()->delete();

        return response()->json([
            "success" => true,
            "user_id" => $user->id,
            "message" => "New user successfully registered"
        ],200);
    }

    public function imgResize($largeImg)
    {
        $path = storage_path().'/app/public/';
        $fileName = Str::random().'.jpg';

        \Tinify\setKey("nF6gz20rpS6RG6qsyF15zKL9PjfqWqCv");
        $source = \Tinify\fromFile($largeImg);
        $resized = $source->resize(array(
            "method" => "thumb",
            "width" => 70,
            "height" => 70
        ));

        $resized->toFile($path.$fileName);

        return $fileName;
    }

    /**
     * Display the specified resource ( view users list ).
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (User::find($id)){
            return response()->json(User::find($id), 200);
        }
        else {
            return response()->json([
                'success' => false,
                'message' => 'User not found'
            ], 404);
        }
    }
}
