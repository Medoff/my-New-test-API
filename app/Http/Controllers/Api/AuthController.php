<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;

class AuthController extends Controller
{
    public function token()
    {
        $user = User::first();
        if(!$user ){
            return response ([
                'message' => ['no such user']
            ],404);
        }
        $token = $user->createToken('user-register-token', ['*'], Carbon::now()->addMinutes(40))->plainTextToken;

        return response()->json([
            'success' => true,
            'token' => $token
        ], 200);
    }
}
