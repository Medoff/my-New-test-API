<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Position;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Position::factory()->create([
            'name' => 'Manager',
        ]);

        Position::factory()->create([
            'name' => 'Designer',
        ]);

        Position::factory()->create([
            'name' => 'Support agent',
        ]);

        Position::factory()->create([
            'name' => 'Security',
        ]);
    }
}
