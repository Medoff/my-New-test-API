<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

</head>
<body>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <a class="btn btn-sm btn-success" href="<?php echo e(route('registerUserForm')); ?>">Новый пользователь</a>
                </div>
                <div class="panel-body">
                    <div class="wrapper list-group-item ">
                        <div class="items"></div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row footer-wrap" style="margin-left:20px">
                        <div class="col-md-2 btn-wrap">
                        </div>
                        <div class="col-md-6 label-wrap">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    document.addEventListener('DOMContentLoaded', (e) => {
        const url = location.protocol + '//' + location.host + '/api/v1/users?count=6';
        getItems(url);
    });

    function getItems(cUrl) {
        const itemsWrapper = document.querySelector('.wrapper .items');
        const buttonWrapper = document.querySelector('.btn-wrap');
        const label = document.querySelector('.label-wrap');

        fetch(cUrl).then((resp) => resp.json()).then((resp) => {
            if (resp.data) {
                let usersCount;
                if (resp.data.users) {
                    resp.data.users.forEach((user) => {
                        let html = '<div style="display: flex;align-items: center;margin-left: 30px;"><div style="margin-right: 20px">' + user.id + '</div><div>' + user.name + '</div></div>';
                        appendHtml(itemsWrapper, html);
                    });
                }
                if (resp.data.links.next_url) {
                    let buttonMore = document.querySelector('.loadmore-button');
                    let usersLable = document.querySelector('.UsersLabel');
                    if (buttonMore) {
                        buttonMore.remove();
                        usersLable.remove();
                    }
                    usersCount = resp.data.page * resp.data.count;
                    let link = resp.data.links.next_url;
                    let html = `<button class="btn loadmore-button" onclick="getItems('${link}')">Load more Button</button>`;
                    appendHtml(buttonWrapper, html);
                } else {
                    document.querySelector('.loadmore-button').remove();
                    document.querySelector('.UsersLabel').remove();
                    usersCount = resp.data.total_users;
                    console.log('no more users to show');
                }
                if (resp.data.total_users) {
                    let html = `<div class="UsersLabel" style="height:34px; margin-top:7px;color: rgb(128, 144, 160);"> Displayed ` + usersCount + ` users from ` + resp.data.total_users + `</div>`
                    appendHtml(label, html);
                }

            }
        }).catch(function (error) {
            console.log(error)
        });
    }

    function appendHtml(el, str) {
        let div = document.createElement('div');
        div.innerHTML = str;
        while (div.children.length > 0) {
            el.appendChild(div.children[0]);
        }
    }
</script>
</body>
</html>
<?php /**PATH D:\myTestLara\resources\views/usersList.blade.php ENDPATH**/ ?>